package be.foreach.stash.hook;

import com.atlassian.stash.content.*;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.mail.MailMessage;
import com.atlassian.stash.mail.MailService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.git.GitCommand;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequestImpl;
import org.apache.commons.lang.StringUtils;

import java.util.*;

public class SmtpNotificationRenderer {

    private static final String BRANCH_CREATION_OR_DELETION = "0000000000000000000000000000000000000000";
    public static final int MAX_PAGE_REQUEST = 1000000;
    private static final String BRANCH_SEP = " / ";
    private static final String REF_ID_TAG = "refs/tags";
    private final MailService mailService;
    private final HistoryService historyService;
    private final NavBuilder navBuilder;
    private final GitCommandBuilderFactory gitCommandBuilderFactory;

    public SmtpNotificationRenderer( final MailService mailService, final HistoryService historyService, final NavBuilder navBuilder, final GitCommandBuilderFactory gitCommandBuilderFactory ) {
        this.mailService = mailService;
        this.historyService = historyService;
        this.navBuilder = navBuilder;
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
    }

    public void sendMail( List<String> emailAddresses, String from, String subject, String body ) {
        for( String emailAddress : emailAddresses ) {
            MailMessage mailMessage = new MailMessage.Builder().to( emailAddress ).from(from).subject(subject).text(body).header("Content-type", "text/html; charset=UTF-8").build();
            mailService.submit( mailMessage );
        }
    }

    public Page<Changeset> getChangesetPage(Repository repository, RefChange refChange) {
        ChangesetsBetweenRequest changesetsBetweenRequest = new ChangesetsBetweenRequest.Builder(repository).exclude(refChange.getFromHash(), new String[0]).include(refChange.getToHash(), new String[0]).build();
        int maxChangeSets = MAX_PAGE_REQUEST;
        if( BRANCH_CREATION_OR_DELETION.equals( refChange.getFromHash() ) ) {
            // When creating and pushing a new branch, don't fetch all changes, limit it to somewhat 100
            maxChangeSets = 100;
        }
        return historyService.getChangesetsBetween(changesetsBetweenRequest, new PageRequestImpl( 0, maxChangeSets ));
    }

    public List<String> getBranches( final Repository repository, final Changeset changeset ) {
        GitStringOutputHandler stringOutputHandler = new GitStringOutputHandler();
        GitCommand<String> gitCommand = gitCommandBuilderFactory.builder( repository ).command( "branch" ).argument( "-a" ).argument( "--contains" ).argument( changeset.getId() ).build(stringOutputHandler);
        String output = gitCommand.call();
        if( output != null ) {
            String branches = StringUtils.stripEnd( output.replaceAll("(\\t|\\r?\\n)+", BRANCH_SEP), BRANCH_SEP );
            String[] numOfBranches = branches.split( BRANCH_SEP );
            return Arrays.asList( numOfBranches );
        } else {
            return Collections.emptyList();
        }
    }

    public boolean isCreatedBranch( RefChange refChange ) {
        return SmtpNotificationRenderer.BRANCH_CREATION_OR_DELETION.equals( refChange.getFromHash() );
    }

    public boolean isDeletedBranch( RefChange refChange ) {
        return SmtpNotificationRenderer.BRANCH_CREATION_OR_DELETION.equals( refChange.getToHash() );
    }

    public boolean isTag( RefChange refChange ) {
        return refChange != null && StringUtils.isNotBlank( refChange.getRefId() ) && refChange.getRefId().startsWith( REF_ID_TAG );
    }

    public String getChangesetUrl( final Repository repository, final Changeset changeset ) {
        return navBuilder.repo( repository ).changeset( changeset.getId() ).buildConfigured();
    }

    public String getMinimalChangesetUrl( final Repository repository, final MinimalChangeset changeset ) {
        return navBuilder.repo( repository ).changeset( changeset.getId() ).buildConfigured();
    }

    public Page<DetailedChangeset> getDetailedChangesetPage(final Repository repository, final Page<Changeset> page, final Changeset changeSet) {
        int maxChangesPerCommit = MAX_PAGE_REQUEST;
        if( page.getSize() > 100 ) {
            // If this is a rather large merge, limit the number of changes to 11, so we can show a ... after the 10th change item
            maxChangesPerCommit = 11;
        }
        DetailedChangesetsRequest.Builder detailedChangesetsRequest = new DetailedChangesetsRequest.Builder( repository ).ignoreMissing(true).maxChangesPerCommit( maxChangesPerCommit );
        detailedChangesetsRequest.changesetId( changeSet.getId() );

        // Make number of change sets configurable
        return historyService.getDetailedChangesets(detailedChangesetsRequest.build(), new PageRequestImpl(0, MAX_PAGE_REQUEST) );
    }

    public Page<? extends Change> getChanges( DetailedChangeset detailedChangeset ) {
        return detailedChangeset.getChanges();
    }

    @SuppressWarnings( "unused" )
    public String getShortHash(String refId) {
        if( refId == null ) return "";
        return refId.length() >= 7 ? refId.substring(0, 7) : refId;
    }
}
